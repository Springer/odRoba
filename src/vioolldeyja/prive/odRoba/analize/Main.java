package vioolldeyja.prive.odRoba.analize;

import vioolldeyja.prive.odRoba.vioolldeyja.prive.odRoba.model.HelperClass;
import vioolldeyja.prive.odRoba.vioolldeyja.prive.odRoba.model.VinNumber;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        List<VinNumber> vinsList = readVinsFromCsv("resources/VINs.csv");
        for(VinNumber number: vinsList){
            HelperClass.checkAndMatchVin(number);
        }
        Collections.sort(vinsList);

        DefaultTableModel model = new DefaultTableModel();
        JTable table = new JTable(model);

        model.addColumn("Vin");
        model.addColumn("valid");
        model.addColumn("issue");
        model.addColumn("manufacturer");

        for(VinNumber v: vinsList){
            model.addRow(new Object[]{v.getVin(), Boolean.toString(v.isVinValid()), v.getValidationIssue(), v.getManufacturer()});
        }
        JFrame f = new JFrame();
        f.setSize(300, 300);
        f.add(new JScrollPane(table));
        f.setVisible(true);

    }


    public static List<VinNumber> readVinsFromCsv(String filesLocation){
        String csvFile = filesLocation;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        List<VinNumber> vinsList = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String vin = line.split(cvsSplitBy)[0].trim();


                VinNumber vinNumber = new VinNumber(vin, false, "");
                vinsList.add(vinNumber);

                System.out.println("Current vin : " + vin + "");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return vinsList;
    }

}
