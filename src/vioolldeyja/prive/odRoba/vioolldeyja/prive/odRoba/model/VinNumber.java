package vioolldeyja.prive.odRoba.vioolldeyja.prive.odRoba.model;

public class VinNumber implements Comparable{
    private String vin;
    private boolean isVinValid;

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    private String manufacturer;

    public String getValidationIssue() {
        return validationIssue;
    }

    public void setValidationIssue(String validationIssue) {
        this.validationIssue = validationIssue;
    }

    private String validationIssue;

    public VinNumber(String vin, boolean vinValid, String validationIssue) {
        this.vin = vin;
        this.isVinValid = vinValid;
        this.validationIssue = validationIssue;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public boolean isVinValid() {
        return isVinValid;
    }

    public void setVinValid(boolean vinValid) {
        isVinValid = vinValid;
    }


    @Override
    public int compareTo(Object o) {
        return ((VinNumber)o).isVinValid()?1:-1;
    }
}
