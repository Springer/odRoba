package vioolldeyja.prive.odRoba.vioolldeyja.prive.odRoba.model;

import vioolldeyja.prive.odRoba.maps.ManufacturerMap;

import java.util.Iterator;
import java.util.Map;

public class HelperClass {

    public static int transliterate(char check){
        if(check == 'A' || check == 'J'){
            return 1;
        } else if(check == 'B' || check == 'K' || check == 'S'){
            return 2;
        } else if(check == 'C' || check == 'L' || check == 'T'){
            return 3;
        } else if(check == 'D' || check == 'M' || check == 'U'){
            return 4;
        } else if(check == 'E' || check == 'N' || check == 'V'){
            return 5;
        } else if(check == 'F' || check == 'W'){
            return 6;
        } else if(check == 'G' || check == 'P' || check == 'X'){
            return 7;
        } else if(check == 'H' || check == 'Y'){
            return 8;
        } else if(check == 'R' || check == 'Z'){
            return 9;
        } else if(Integer.valueOf(Character.getNumericValue(check)) != null){ //hacky but works
            return Character.getNumericValue(check);
        }
        return -1;
    }

    public static boolean isVinValid(String dataRow) {
        int[] values = { 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 0, 7, 0, 9,
                2, 3, 4, 5, 6, 7, 8, 9 };
        int[] weights = { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };


        String s = dataRow;
        s = s.replaceAll("-", "");
        s = s.replaceAll(" ", "");
        s = s.toUpperCase();
        if (s.length() != 17)
            throw new RuntimeException("VIN number must be 17 characters");

        int sum = 0;
        for (int i = 0; i < 17; i++) {
            char c = s.charAt(i);
            int value;
            int weight = weights[i];

            // letter
            if (c >= 'A' && c <= 'Z') {
                value = values[c - 'A'];
                if (value == 0)
                    throw new RuntimeException("Illegal character: " + c);
            }

            // number
            else if (c >= '0' && c <= '9')
                value = c - '0';

                // illegal character
            else
                throw new RuntimeException("Illegal character: " + c);

            sum = sum + weight * value;

        }

        // check digit
        sum = sum % 11;
        char check = s.charAt(8);
        if (sum == 10 && check == 'X') {
            System.out.println("Valid");
            return true;
        } else if (sum == transliterate(check)) {
            System.out.println("Valid");
            return true;
        } else {
            System.out.println("Invalid");
            return false;
        }

    }

    public static String matchManufacturer(VinNumber vin){
        String manufacturer = "";

        Iterator it = ManufacturerMap.getManufacturersCodes().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(vin.getVin().substring(0,3).equalsIgnoreCase((String) pair.getKey())){
                return (String) pair.getValue();
            }
            it.remove();
        }
        return "No manufacturer found";
    }

    public static void checkAndMatchVin(VinNumber vin){
        boolean vinValid = false;
        String validationIssue = "";
        String manufacturer = "";
        try {
            vinValid = HelperClass.isVinValid(vin.getVin());
            validationIssue=vinValid?"":"Invalid sumcheck";
            manufacturer = matchManufacturer(vin);
        }catch(Exception ex){
            validationIssue = ex.getMessage();
            System.out.println("validationIssue: " + validationIssue);
        }
        vin.setVinValid(vinValid);
        vin.setValidationIssue(validationIssue);
        vin.setManufacturer(manufacturer);
    }
}
